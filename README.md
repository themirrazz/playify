# Playify
Wrapper around a modded version of Spotify Web Player

## Sample Program

```js
var player=new Playify("My Player")

var socket=io("ws://localhost:30/buttons")

socket.on("tgpp",() => {
  if(player.isPlaying()) player.pause();
  else player.play()
})




